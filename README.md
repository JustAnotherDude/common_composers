
# COMMON COMPOSER FILES
Here i will collect common docker-compose config files, which i regularly use.

## Required actions before building
* First, just copy file _global_secrets.example.env_ to _global_secrets.env_ and change env variables.
* ???
* **PROFIT**

## Main Devtools Set
### Services
* **PostgreSQL** (10 version on Alpine)
* **Redis** (version on Alpine 3.8)
* **PGAdmin** (just latest version)
* **PGHero** (just latest version)

### Ports in use:
- 5432 is for PostgreSQL
- 3000 is for PGAdmin
- 6379 is for Redis
- 8080 is for PGHero

### Global Secrets
Need to setup environment variable for **PGHero**. Here's example:
```bash
DATABASE_URL=postgres://vanyaz158@postgres:5432/active_database
```
